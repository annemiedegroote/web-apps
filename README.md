# README #

Welcome to Pew Pew News!

### What is this repository for? ###

* This is the first version of the website for gaming info.
* There will be extra functionalities soon!

### How do I get set up? ###

* npm install in the root (you might need admin/su)
* start mongo
* gulp runTests for testing

### What has changed? ###

* Downvotes
* Angular Material
* Adjusted colors/icons 
* Slider with animation
* Extra test
* Refactoring (limited)
* Gitflow