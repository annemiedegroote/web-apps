angular
	.module('pewpewNews')
	.controller('AppCtrl', AppCtrl);


function AppCtrl($scope, $mdSidenav) {
	$scope.toggleSidenav = function (menuId) {
		$mdSidenav(menuId).toggle();
	};

	$scope.menu = [
		{
			link: '#/home',
			title: 'Home',
			icon: 'dashboard'
		}
	];


}