angular
	.module('pewpewNews')
	.controller('MainCtrl', MainCtrl);
	
function MainCtrl($scope, posts, auth){
	$scope.posts = posts.posts;
    $scope.isLoggedIn = auth.isLoggedIn;

    $scope.addPost = function () {
        if (!$scope.title || $scope.title === '') {
            return;
        }
        posts.create({
            title: $scope.title,
            link: $scope.link,
        });
        $scope.title = '';
        $scope.link = '';
    };

    $scope.incrementUpvotes = function (post) {
        posts.upvote(post);
    }

    $scope.decreaseDownvotes = function (post) {
        posts.downvote(post);
    }
}