angular
	.module('pewpewNews')
	.controller('PostsCtrl', PostsCtrl);

function PostsCtrl($scope, posts, post, auth) {
	$scope.post = post;
    $scope.isLoggedIn = auth.isLoggedIn;

    $scope.addComment = function () {
		if (!$scope.body || $scope.body === '') {
			return;
		}

		posts.addComment(post._id, {
			body: $scope.body,
			author: 'user',
		}).success(function (comment) {
			$scope.post.comments.push(comment);
		});

		$scope.body = '';
    }

    $scope.incrementUpvotes = function (comment) {
		posts.upvoteComment(post, comment);
    }

	$scope.decreaseDownvotes = function (comment) {
		posts.downvoteComment(post, comment);
	}
}