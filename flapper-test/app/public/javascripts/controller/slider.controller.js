angular
  .module('pewpewNews')
  .controller('SliderCtrl', SliderCtrl);

function SliderCtrl($scope) {
  $scope.images = [{
    src: 'ff.png',
    title: 'Final Fantasy'
  }, {
      src: 'lol.jpg',
      title: 'League of Legends'
    }, {
      src: 'wow.jpg',
      title: 'World of Warcraft'
    }, {
      src: 'zelda.jpg',
      title: 'The Legend of Zelda'
    }];
};