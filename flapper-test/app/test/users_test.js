var expect = require('chai').expect;
var app = require('../app');
var request = require('supertest');
var agent = request.agent(app);

describe('POST /login', function () {
    it('should respond with 200 in case of valid login post, use "mieke" and "testje"!', function (done) {
        agent.post('/login')
            .send({
                'username': 'mieke',
                'password': 'testje'
            }).expect(200)
            .end(function (err, res) {
                if (err) { return done(err); }

                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.not.empty;

                if (fetchedData) {
                    expect(fetchedData).to.have.all.keys('token');
                    done();
                }
            });
    });
});